declare module '*?url' {
  const assetUrl: string
  export default assetUrl
}

declare module '*?raw' {
  const assetsRaw: string
  export default assetsRaw
}

declare module '*?inline' {
  const assetsInline: string
  export default assetsInline
}

// node.js child_process
declare module '*?fork' {
  import { ChildProcess, ForkOptions } from 'child_process'

  function fork(options?: ForkOptions): ChildProcess
  function fork(args?: ReadonlyArray<string>, options?: ForkOptions): ChildProcess

  export default fork
}

// node.js worker
declare module '*?worker' {
  import { WorkerOptions, Worker } from 'worker_threads'
  const workerConstructor: new (options?: WorkerOptions) => Worker
  export default workerConstructor
}

// shareworker
declare module '*?sharedworker' {
  const sharedWorkerConstructor: new (options?: WorkerOptions) => SharedWorker
  export default sharedWorkerConstructor
}