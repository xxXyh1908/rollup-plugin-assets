import { Plugin } from 'rollup';

declare type Compressor = (source: Buffer | string, filename: string) => Buffer | string | Promise<Buffer | string>;
interface UserOptions {
    publicPath?: string | {
        [src: string]: string;
    } | (string | {
        [src: string]: string;
    })[];
    include?: string | RegExp | (string | RegExp)[];
    exclude?: string | RegExp | (string | RegExp)[];
    inline?: boolean | number | ((filename: string) => boolean | Promise<boolean>);
    resolveFileNameExpression?: boolean;
    target?: 'node';
    root?: string;
    minify?: boolean | ((filename: string) => boolean | Promise<boolean>);
    minifyPlugins?: {
        [extName: string]: Compressor;
    };
    base?: string;
    native?: boolean;
}

declare const DEFAULT_ASSETS_INCLUDE_RE: RegExp;

declare function assets(options?: UserOptions): Plugin;

export { Compressor, UserOptions, assets as default, DEFAULT_ASSETS_INCLUDE_RE as defaultInclude };
