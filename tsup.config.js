const pkg = require('./package.json')
const { defineConfig } = require('tsup')

module.exports = defineConfig({
  clean: true,
  dts: true,
  outDir: 'dist',
  external: [...Object.keys(pkg.dependencies ?? {}), ...Object.keys(pkg.peerDependencies ?? {})],
  format: ['cjs'],
  entryPoints: ['src/index.ts'],
  // sourcemap: true,
  minifySyntax: true,
  minifyWhitespace: true,
})
