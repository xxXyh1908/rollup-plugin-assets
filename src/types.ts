export type Compressor = (source: Buffer | string, filename: string) => Buffer | string | Promise<Buffer | string>

export interface UserOptions {
  publicPath?: string | { [src: string]: string } | (string | { [src: string]: string })[]
  include?: string | RegExp | (string | RegExp)[]
  exclude?: string | RegExp | (string | RegExp)[]
  inline?: boolean | number | ((filename: string) => boolean | Promise<boolean>)
  resolveFileNameExpression?: boolean
  target?: 'node'
  root?: string
  minify?: boolean | ((filename: string) => boolean | Promise<boolean>)
  minifyPlugins?: { [extName: string]: Compressor }
  base?: string
  native?: boolean
}
