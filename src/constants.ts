// ** READ THIS ** before editing `KNOWN_ASSET_TYPES`.
//   If you add an asset to `KNOWN_ASSET_TYPES`, make sure to also add it
export const KNOWN_ASSET_TYPES = [
  // images
  'png',
  'jpe?g',
  'gif',
  'svg',
  'ico',
  'webp',
  'avif',

  // media
  'mp4',
  'webm',
  'ogg',
  'mp3',
  'wav',
  'flac',
  'aac',

  // plain
  'txt'
]

export const KNOWN_PREPROCESSING_TYPES = [
  '\\w+ss',
  'styl',
  'vue',
  'svelte',
  'wxs',
  'ejs',
  '\\w+html?',
  '\\w+ml',
  'md',
  'tpl',
  'template',
  'hbs',
  'module\\.\\w+'
]

const KNOWN_PREPROCESSING_TYPES_RE = new RegExp(`\\.(` + KNOWN_PREPROCESSING_TYPES.join('|') + `)$`, 'i')

export const isPreprocessorType = (source: string) => {
  if(JS_TYPES_RE.test(source)) return false
  if(KNOWN_PREPROCESSING_TYPES_RE.test(source)) return true
  return false
}

export const DEFAULT_EXTENSIONS = ['.ts', '.tsx', '.jsx', '.mjs', '.cjs', '.js', '.node']
export const DEFAULT_ASSETS_INCLUDE_RE = new RegExp(`\\.(` + KNOWN_ASSET_TYPES.join('|') + `)$`, 'i')

export const JS_TYPES_RE = /\.([jt]sx?|[mc]js)$/i

export const COMMONJS_REQUIRE = '?commonjs-require'
export const CWD = process.cwd()
