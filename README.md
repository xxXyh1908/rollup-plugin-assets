<h1 align="center">rollup-plugin-assets 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-2.0.1-blue.svg?cacheSeconds=2592000" />
  <img alt="rollup" src="https://img.shields.io/badge/rollup-2.5.0-orange.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> Rollup plugin to rebase, inline assets referenced from the JavaScript code.

### 🏠 [Homepage](https://gitee.com/xxXyh1908/rollup-plugin-assets/#readme)

## install

```bash
npm i -D @xyh19/rollup-plugin-assets
# or
yarn add -D @xyh19/rollup-plugin-assets
```

## Getting Started

Create a `rollup.config.js` [configuration file](https://www.rollupjs.org/guide/en/#configuration-files) and import the plugin:

```js
// rollup.config.js
import assets, { defaultInclude } from '@xyh19/rollup-plugin-assets'
import path from 'path'

export default {
  input: 'src/index.js',
  output: {
    dir: 'output',
    format: 'cjs',
  },
  plugins: [
    assets({
      publicPath: 'public/',
      include: [defaultInclude, 'src/assets/**']
      minify: false
    }),
  ],
}
```

## Configuration

### publicPath
- **Type:** `string | { [src: string]: string } | (string | { [src: string]: string })[]`

Directory to serve as plain static assets. Files in this directory are served at / during dev and copied to the root of outDir during build, and are always served or copied as-is without transform. The value can be either an absolute file system path or a path relative to project root.

### include
- **Type:** `string | RegExp | (string | RegExp)[]`
- **Default:** `defaultInclude`

An array of glob patterns to include matches.

### exclude
- **Type:** `string | RegExp | (string | RegExp)[]`
- **Default:** `[]`

An array of glob patterns to exclude matches.

### inline
- **Type:** `boolean | number | ((filename: string) => boolean | Promise<boolean>)`

Inline assets as base64 urls directly in source code.

### target
- **Type:** `undefined | 'node'`

### root
- **Type:** `string`
- **Default:** `process.cwd()`

### base
- **Type:** `string | undefined`
Base public path.

### minify
- **Type:** `boolean | ((filename: string) => boolean | Promise<boolean>)`

Compress static assets.

### minifyPlugins
- **Type:** `{ [extName: string]: Compressor }`

Compress plugin.

### native(new)
- **Type:** `boolean`

load (*.node) file.
It needs to be used with [@rollup/plugin-commonjs](https://www.npmjs.com/package/@rollup/plugin-commonjs).

### resolveFileNameExpression
- **Type:** `boolean`
- **Default:** `true`

Resolve filename-expression.
```js
const path1 = __dirname + '/1.jpg'                // ok
const path2 = __dirname + '/music/' + '/1.mp3'    // ok
const path3 = __filename + '/../1.jpg'            // ok
const path4 = `${__dirname}/css/index.css`        // ok
const path5 = path.join(__dirname, '1.jpg')       // ok
const path5 = require.resolve('./1.jpg')       // ok

const path6 = path.dirname(__filename) + '/1.jpg' // failed
```

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitee.com/xxXyh1908/rollup-plugin-assets/issues).

## Give a ⭐️ if this project helped you!